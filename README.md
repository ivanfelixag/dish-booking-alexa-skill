# Alexa Skill - Dish Booking

This project contains an Alexa Skill that let you booking a plate.

## Alexa Skill Kit Commands

The lifecycle of the skill developing process is:

- You need to initialize the profile with the AWS Client Id and AWS Secret Id.
```
ask init
```

- Create a new skill (create the project folder)
```
ask new
```

- Once you have been configured or added new capacities to your skill, you have to deploy it.
```
ask deploy
```

- To debug our dialog we need to introduce the command below:
```
ask dialog --locale es-ES
```

## Important files and folders

When you introduce the command **ask new**, the ask will create all files and folders you need to start to program your skill. The most important files and folder that you need to have in your mind are:
```
.
├── skill.json
├── models
|   ├── es-ES.json
|   ├── en-EU.json
|   └── xx-XX.json
└── lambda
    └── custom
        ├── index.js
        ├── languageStrings.js
        └── package.json

```

### **skill.json**
This file define two important parts:

**publishingInformation / locales**: Alexa App will show the skill description for each configured language.

**apis**: all api resources the skill will consume.

### **models folder** 
Intent, slots and utterances definitions for each configured language.

**What is an intent?**:
Intent is a skill piece which goal is define an action. Each intent is associated with an intent handler in *lambda/custom/index.js*

**What are slots?**:
They are a intent variables which posible values are defined inside [Amazon Slot Types](https://developer.amazon.com/es/docs/custom-skills/slot-type-reference.html) or your custom types.

**What are utterances?**: They are the different forms that you let an user invoke your skill actions.

### **lambda folder**
This folder contains the skill actions logic.

**package.json**: has the description and lambda dependencies.

**languageStrings.js**: contains all strings your intent handlers use for each language.

**index.js**: all skill actions logic are here. This file define an intent handler for each intent in the model file. Remember, each model file has the same intents that the other model files.

## Session attributes

You can share data between intents using session attributes. You can see an example inside the intents **DishBookingIntentHandler** and **WhenBookingIntentHandler** in [index.js](./lambda/custom/index.js)

## Example in Alexa Developer Console 

![dish booking alexa console](./dish-booking-alexa-console.png "dish booking alexa console")