/* *
 * We create a language strings object containing all of our strings.
 * The keys for each string will then be referenced in our code, e.g. handlerInput.t('WELCOME_MSG').
 * The localisation interceptor in index.js will automatically choose the strings
 * that match the request's locale.
 * */

module.exports = {
    es: {
        translation: {
            WELCOME_MSG: 'Un placer hablar de nuevo contigo, ¿qué plato desea reservar?',
            DISH_TYPES_MSG: 'Los tipos de platos que tenemos actualmente son pescado del día, carne del día o verduras',
            MORE_INFO_DISH_TYPES_MSG: 'Lo que tenemos es ',
            MORE_INFO_DISH_TYPES_ERROR_MSG: 'Disculpa, no he entendido el tipo que quiere consultar',
            NEED_DISH_BOOKING_MSG: 'Por favor, indícame que plato deseas reservar antes de continuar',
            WHEN_DISH_BOOKING_MSG: '¿Para cuándo desea realizar la reserva?',
            DISH_BOOKING_MSG: 'Se ha reservado {dish} para {bookingDate}',
            FISH_MSG: 'lubina al horno',
            MEAT_MSG: 'cachopo asturiano',
            VEGETABLE_MSG: 'espinacas',
            HELP_MSG: 'Puedes reservar un tipo de plato, di la frase quiero conocer los tipos de platos para saber qué tipos de platos puedes reservar',
            GOODBYE_MSG: 'Hasta luego!',
            REFLECTOR_MSG: 'Acabas de activar {{intentName}}',
            FALLBACK_MSG: 'Lo siento, no se nada sobre eso. Por favor inténtalo otra vez.',
            ERROR_MSG: 'Lo siento, ha habido un error. Por favor inténtalo otra vez.'
        }
    }
}